#!/bin/bash
SERVERID=$1
SPACENAME=$2

DST='s3://'$SPACENAME'/backups/database/'$SERVERID'/'
DAYSTOKEEP=30

clearOldBackups(){
	echo "##### DELETING OLD BACKUPS ######"
	OBJECTSLIST=$(s3cmd ls $DST | grep -oP 's3:.*')

	while [ $DAYSTOKEEP -lt 30 ]; do
	  # Compose days based on the iterator's current DAYSTOKEEP value.
	  DATE=$(date -d "now -"$DAYSTOKEEP" days" +%d%m%y)
	  FOUNDDATE=$(echo -e $OBJECTSLIST | grep -oP $DATE)
	  if [ "$FOUNDDATE" != "" ]; then
		# Compose directory to delete and make call to recursively delete
		# all objects in the directory.
		PATH_TO_DELETE_OBJECT=$DST$FOUNDDATE
		s3cmd del --recursive $PATH_TO_DELETE_OBJECT
	  fi
	  let DAYSTOKEEP=DAYSTOKEEP+1
	done
	echo "##### DONE DELETING BACKUPS #####"
}

clearOldBackups
